<?php
# This file was automatically generated by the MediaWiki 1.34.0
# installer. If you make manual changes, please keep track in case you
# need to recreate them later.
#
# See includes/DefaultSettings.php for all configurable settings
# and their default values, but don't forget to make changes in _this_
# file, not there.
#
# Further documentation for configuration settings may be found at:
# https://www.mediawiki.org/wiki/Manual:Configuration_settings

# Protect against web entry
if ( !defined( 'MEDIAWIKI' ) ) {
	exit;
}

## Uncomment this to disable output compression
# $wgDisableOutputCompression = true;

$wgSitename = "Manjaro Wiki";
$wgMetaNamespace = "Manjaro_Wiki";

## The URL base path to the directory containing the wiki;
## defaults for all runtime URL paths are based off of this.
## For more information on customizing the URLs
## (like /w/index.php/Page_title to /wiki/Page_title) please see:
## https://www.mediawiki.org/wiki/Manual:Short_URL
$wgScriptPath = "";

## The protocol and server name to use in fully-qualified URLs
$wgServer = "http://localhost";

## The URL path to static resources (images, scripts, etc.)
$wgResourceBasePath = $wgScriptPath;

## The URL path to the logo.  Make sure you change this from the default,
## or else you'll overwrite your logo when you upgrade!
$wgLogo = "$wgResourceBasePath/resources/assets/wiki.png";

## UPO means: this is also a user preference option

$wgEnableEmail = true;
$wgEnableUserEmail = true; # UPO

$wgEmergencyContact = "webmaster@manjaro.org";
$wgPasswordSender = "webmaster@manjaro.org";

$wgEnotifUserTalk = false; # UPO
$wgEnotifWatchlist = false; # UPO
$wgEmailAuthentication = true;

## Database settings
$wgDBtype = "mysql";
$wgDBserver = "localhost";
$wgDBname = "XXX";
$wgDBuser = "XXX";
$wgDBpassword = "";

# MySQL specific settings
$wgDBprefix = "";

# MySQL table options to use during installation or update
$wgDBTableOptions = "ENGINE=InnoDB, DEFAULT CHARSET=binary";

## Shared memory settings
$wgMainCacheType = CACHE_NONE;
$wgMemCachedServers = [];

## To enable image uploads, make sure the 'images' directory
## is writable, then set this to true:
$wgEnableUploads = true;
$wgUseImageMagick = true;
$wgImageMagickConvertCommand = "/usr/bin/convert";

# InstantCommons allows wiki to use images from https://commons.wikimedia.org
$wgUseInstantCommons = false;

# Periodically send a pingback to https://www.mediawiki.org/ with basic data
# about this MediaWiki instance. The Wikimedia Foundation shares this data
# with MediaWiki developers to help guide future development efforts.
$wgPingback = false;

## If you use ImageMagick (or any other shell command) on a
## Linux server, this will need to be set to the name of an
## available UTF-8 locale
$wgShellLocale = "en_US.utf8";

## Set $wgCacheDirectory to a writable directory on the web server
## to make your wiki go slightly faster. The directory should not
## be publicly accessible from the web.
#$wgCacheDirectory = "$IP/cache";

# Site language code, should be one of the list in ./languages/data/Names.php
$wgLanguageCode = "en";

$wgSecretKey = "XXX";

# Changing this will log out all existing sessions.
$wgAuthenticationTokenVersion = "1";

# Site upgrade key. Must be set to a string (default provided) to turn on the
# web installer while LocalSettings.php is in place
$wgUpgradeKey = "XXX";

## Paste below codes to your local settings **************************************************************************************

## For attaching licensing metadata to pages, and displaying an
## appropriate copyright notice / icon. GNU Free Documentation
## License and Creative Commons licenses are supported so far.
$wgRightsPage = ""; # Set to the title of a wiki page that describes your license/copyright
$wgRightsUrl = "http://www.gnu.org/copyleft/fdl.html";
$wgRightsText = "GNU Free Documentation License 1.3 or later";
$wgRightsIcon = "$wgScriptPath/resources/assets/licenses/gnu-fdl.png";

# Path to the GNU diff3 utility. Used for conflict resolution.
$wgDiff3 = "/usr/bin/diff3";

## Default skin: you can change the default skin. Use the internal symbolic
## names, ie 'vector', 'monobook':
$wgDefaultSkin = "vector";

#inspired from arch wiki settings. thanks a lot to Arch dev teams.

$wgShowIPinHeader = false;
$wgEnableSidebarCache = true;
$wgUseFileCache = true;
$wgUserEmailUseReplyTo = true;
#$wgFileCacheDirectory = "$IP/../cache/html";
$wgUseGzip = true;
$wgUseETag = true;

# CSS-based preferences supposedly cause about 20 times slower page loads
# https://phabricator.wikimedia.org/rSVN63707
$wgAllowUserCssPrefs = false;


# Enable support for userscripts and user-stylesheets
$wgAllowUserJs = true;
$wgAllowUserCss = true;

##
## User roles
##
## User roles are groups without permissions: they only serve to clarify the
## main function of the user in the wiki, and they must be associated to
## specific access levels (see below) as needed.
##

$wgGroupPermissions['maintainer'] = array();
$wgGroupPermissions['translator'] = array();

## Access levels
##
## These groups shouldn't be used to define the _role_ of users, but only the
## extent of the permissions that they have in the wiki; if a user is given
## an access level greater than 'user', their role (see above) must be
## specified as well.
##

# disable anonymous editing
$wgEmailConfirmToEdit = false;#Should be true when built product
$wgDisableAnonTalk = true;
$wgGroupPermissions['*']['edit'] = false;

# extra rights for sysop
$wgGroupPermissions['sysop']['deleterevision']  = true;

# disable uploads by normal users
$wgGroupPermissions['user']['upload']          = false;
$wgGroupPermissions['user']['reupload']        = false;
$wgGroupPermissions['user']['reupload-shared'] = false;
$wgGroupPermissions['autoconfirmed']['upload'] = false;

# co-sysop's rights
$wgGroupPermissions['cosysop']['autopatrol'] = true;
$wgGroupPermissions['cosysop']['patrol'] = true;
$wgGroupPermissions['cosysop']['noratelimit'] = true;
$wgGroupPermissions['cosysop']['suppressredirect'] = true;
$wgGroupPermissions['cosysop']['rollback'] = true;
$wgGroupPermissions['cosysop']['browsearchive'] = true;
$wgGroupPermissions['cosysop']['apihighlimits'] = true;
$wgGroupPermissions['cosysop']['unwatchedpages'] = true;
$wgGroupPermissions['cosysop']['deletedhistory'] = true;
$wgGroupPermissions['cosysop']['deletedtext'] = true;

# additional page-protection levels
$wgRestrictionLevels[] = 'editprotected2';
$wgGroupPermissions['sysop']['editprotected2'] = true;
$wgGroupPermissions['cosysop']['editprotected2'] = true;
$wgEnableWriteAPI = true;

# enable edits by normal users(Moderation extension prevents the vandalism)
$wgGroupPermissions['user']['edit']=true;

# disable user account creation via API
$wgAPIModules['createaccount'] = 'ApiDisabled';

# remove 'writeapi' right from anonymous
$wgGroupPermissions['*']['writeapi'] = false;
$wgGroupPermissions['user']['writeapi'] = true;

# add 'writeapi' to autoconfirmed users
$wgGroupPermissions['autoconfirmed']['writeapi'] = true;

# stricter conditions for 'autoconfirmed' promotion
$wgAutoConfirmAge = 86400*3; // three days

# require at least 20 normal edits before granting the 'writeapi' right
$wgAutoConfirmCount = 20;

# Enforce basic editing etiquette
# We set the defaults for "minordefault" (disabled) and "forceeditsummary"
# (enabled) options and hide them from the user preferences dialog. Note that
# hiding the user preferences with $wgHiddenPrefs results in everybody using
# the defaults, regardless of the users' earlier preference.
$wgDefaultUserOptions["minordefault"] = 0;
$wgDefaultUserOptions["forceeditsummary"] = 1;
$wgHiddenPrefs[] = "minordefault";
$wgHiddenPrefs[] = "forceeditsummary";

# Enabled skins.
# The following skins were automatically enabled:
wfLoadSkin( 'MonoBook' );
wfLoadSkin( 'Timeless' );
wfLoadSkin( 'Vector' );
wfLoadSkin( 'MinervaNeue' );
wfLoadSkin( 'Tweeki' );
$wgTweekiSkinUseAwesome = true;
$wgTweekiSkinCustomEditSectionLink=false;

# Enabled extensions. Most of the extensions are enabled by adding
# wfLoadExtensions('ExtensionName');
# to LocalSettings.php. Check specific extension documentation for more details.
# The following extensions were automatically enabled:
wfLoadExtension( 'CategoryTree' );
wfLoadExtension( 'CodeEditor' );
wfLoadExtension( 'MultimediaViewer' );
wfLoadExtension( 'PageImages' );
wfLoadExtension( 'SyntaxHighlight_GeSHi' );
wfLoadExtension( 'WikiEditor' );
wfLoadExtension( 'FontAwesome' );

# Translation extensions

wfLoadExtension( 'Babel' );
wfLoadExtension( 'cldr' );
wfLoadExtension( 'CleanChanges' );
wfLoadExtension( 'LocalisationUpdate' );
wfLoadExtension( 'Translate' );
wfLoadExtension( 'UniversalLanguageSelector' );

# Translation configs

$wgCCTrailerFilter = true;
$wgCCUserFilter = false;
$wgDefaultUserOptions['usenewrc'] = 1;
$wgLocalisationUpdateDirectory = "$IP/cache";
$wgGroupPermissions['user']['translate'] = true;
$wgGroupPermissions['user']['translate-messagereview'] = true;
$wgGroupPermissions['user']['translate-groupreview'] = true;
$wgGroupPermissions['translator']['translate-import'] = true;
$wgGroupPermissions['sysop']['pagetranslation'] = true;
$wgGroupPermissions['sysop']['translate-manage'] = true;
$wgGroupPermissions['moderator']['pagetranslation'] = true;
$wgGroupPermissions['moderator']['translate-manage'] = true;
$wgTranslateDocumentationLanguageCode = 'qqq';

# TitleBlackList Extension
wfLoadExtension( 'TitleBlacklist' );
$wgTitleBlacklistSources = array(
    array(
         'type' => 'localpage',
         'src'  => 'MediaWiki:Titleblacklist',
    ),
    array(
         'type' => 'url',
         'src'  => 'https://meta.wikimedia.org/w/index.php?title=Title_blacklist&action=raw',
    )
);


# BlackList Extension
wfLoadExtension( 'SpamBlacklist' );
$wgBlacklistSettings = [
	'spam' => [
		'files' => [
			"https://meta.wikimedia.org/w/index.php?title=Spam_blacklist&action=raw&sb_ver=1",
			"https://en.wikipedia.org/w/index.php?title=MediaWiki:Spam-blacklist&action=raw&sb_ver=1"
		],
	],
];

/**
 * Name of the bot which will invalidate translations and do maintenance
 * for page translation feature. Also used for importing messages from external
 * sources.
 */

$wgTranslateFuzzyBotName = 'FuzzyBot';

/**
 * Text that will be shown in translations if the translation is outdated.
 * Must be something that does not conflict with actual content.
 */

if ( !defined( 'TRANSLATE_FUZZY' ) ) {
	define( 'TRANSLATE_FUZZY', '!!FUZZY!!' );
}

/**
 * When unprivileged users open a translation editor, they will
 * see a message stating that a special permission is needed for translating
 * messages. If this variable is defined, there is a button which will
 * take the user to that page to ask for permission.
 * The target needs to be reiterated with the second variable to have
 * the same result with sandbox enabled where users can't enter the sandbox.
 */

$wgTranslatePermissionUrl = 'Project:User';
$wgTranslateSecondaryPermissionUrl = 'Project:User';

/**
 * Enable page translation feature.
 *
 * Page translation feature allows structured translation of wiki pages
 * with simple markup and automatic tracking of changes.
 *
 * @defgroup PageTranslation Page Translation
 * @see https://www.mediawiki.org/wiki/Help:Extension:Translate/Page_translation_administration
 */

$wgEnablePageTranslation = true;

/**
 * List of possible message group review workflow states and properties
 * for each state.
 * The currently supported properties are:
 * * color: the color that is used for displaying the state in the tables.
 * * right: additional right that is needed to set the state.
 * Users who have the translate-groupreview right can set this in
 * Special:Translate.
 * The state is visible in Special:Translate, Special:MessageGroupStats and
 * Special:LanguageStats.
 * If the value is false, the workflow states feature is disabled.
 * State name can be up to 32 characters maximum.
 * Example:
 * $wgTranslateWorkflowStates = array(
 *      'new' => array( 'color' => 'FF0000' ), // red
 *      'needs_proofreading' => array( 'color' => '0000FF' ), // blue
 *      'ready' => array( 'color' => 'FFFF00' ), // yellow
 *      'published' => array(
 *          'color' => '00FF00', // green
 *          'right' => 'centralnotice-admin',
 *      ),
 * );
 */

$wgTranslateWorkflowStates = array(
	    'new' => array( 'color' => 'FF0000' ), // red
	    'needs_proofreading' => array( 'color' => '0000FF' ), // blue
	    'ready' => array( 'color' => 'FFFF00' ), // yellow
	    'published' => array(
	        'color' => '00FF00', // green
	        'right' => 'centralnotice-admin',
	),
);

# Add a preference "Do not send me email newsletters" in the email preferences.

$wgTranslateNewsletterPreference = false;

# Captcha Extension

wfLoadExtensions([ 'ConfirmEdit', 'ConfirmEdit/ReCaptchaNoCaptcha' ]);
$wgCaptchaClass = 'ReCaptchaNoCaptcha';
$wgReCaptchaSiteKey = 'your public/site key here';
$wgReCaptchaSecretKey = 'your private key here';

$wgCaptchaTriggers['edit'] = false;
$wgCaptchaTriggers['create'] = true;
$wgCaptchaTriggers['addurl'] = true;
$wgCaptchaTriggers['createaccount'] = true;
$wgCaptchaTriggers['badlogin'] = true;

# Check User Extension

wfLoadExtension( 'CheckUser' );
$wgGroupPermissions['sysop']['checkuser'] = true;
$wgGroupPermissions['sysop']['checkuser-log'] = true;

# Parser Function Extension

wfLoadExtension( 'ParserFunctions' );

# Interwiki Extension

wfLoadExtension( 'Interwiki' );
$wgGroupPermissions['sysop']['interwiki'] = true;

# Titlekey Extension

wfLoadExtension( 'TitleKey' );

# Lockdown Extension to prevent to enter some page

wfLoadExtension( 'Lockdown' );
$wgSpecialPageLockdown['Recentchanges'] = [ 'user' ];
$wgSpecialPageLockdown['Newpages'] = [ 'user' ];
$wgSpecialPageLockdown['Recentchangeslinked'] = [ 'user' ];
$wgSpecialPageLockdown['Log'] = [ 'user' ];
$wgSpecialPageLockdown['Diff'] = [ 'user' ];
$wgActionLockdown['history'] = ['user'];

# Abuse filter Extension to allows privileged users to set specific actions to be taken when actions by users, such as edits, match certain criteria. 

# Need configure

wfLoadExtension( 'AbuseFilter' );
$wgGroupPermissions['sysop']['abusefilter-modify'] = true;
$wgGroupPermissions['*']['abusefilter-log-detail'] = true;
$wgGroupPermissions['*']['abusefilter-view'] = true;
$wgGroupPermissions['*']['abusefilter-log'] = true;
$wgGroupPermissions['sysop']['abusefilter-privatedetails'] = true;
$wgGroupPermissions['sysop']['abusefilter-modify-restricted'] = true;
$wgGroupPermissions['sysop']['abusefilter-revert'] = true;

# Example of abusefilter:https://en.wikipedia.org/wiki/Special:AbuseFilter

# Nuke Extension to makes it possible for sysops to mass delete pages.

wfLoadExtension( 'Nuke' );
$wgGroupPermissions['sysop']['nuke'] = false;
$wgGroupPermissions['nuke']['nuke'] = true;

# Debug

$wgShowExceptionDetails = true;

# Codemirror Extension

wfLoadExtension( 'CodeMirror' );

# Enables use of CodeMirror by default but still allow users to disable it
$wgDefaultUserOptions['usecodemirror'] = 1;

# Thanks Extension

wfLoadExtension( 'Thanks' );

# Echo Extension

wfLoadExtension( 'Echo' );

# Translation Notification

wfLoadExtension( 'TranslationNotifications' );

$wgTranslationNotificationsContactMethods = array(
	'email' => true,
	'talkpage' => true,
	'talkpage-elsewhere' => false,
);

$wgGroupPermissions['sysop']['translate-manage'] = true;

# Mobile Support

wfLoadExtension( 'MobileFrontend' );

# Cookie Extension

wfLoadExtension( 'CookieWarning' );

$wgCookieWarningEnabled = true;

# Preloader for a new page template

wfLoadExtension( 'Preloader' );

$wgPreloaderSource[NS_MAIN] = 'Template:Boilerplate';

# Set Logo

$wgLogo = $wgScriptPath . '/images/logo.png';


# Moderation Extension

wfLoadExtension( 'Moderation' );

$wgGroupPermissions['automoderated']['skip-move-moderation'] = false;
$wgGroupPermissions['sysop']['skip-move-moderation'] = true;
$wgGroupPermissions['moderator']['skip-move-moderation'] = true;
$wgGroupPermissions['moderator']['skip-moderation'] = true;
$wgGroupPermissions['bot']['skip-move-moderation'] = true;
$wgGroupPermissions['bot']['skip-moderation'] = true;
# Registering new accounts with offensive names is still a way for a vandal to show itself in the RecentChanges. A simple solution is to remove newusers log from RecentChanges: 
$wgLogRestrictions["newusers"] = 'moderation';

$wgSMTP = array(
	'host'     => "manjaro-wiki_smtp_1", // could also be an IP address. Where the SMTP server is located
	'IDHost'   => "localhost",     // Generally this will be the domain name of your website (aka mywiki.org)
	'port'     => 25,               // Port to use when connecting to the SMTP server
	'auth'     => false             // Should we use SMTP authentication (true or false)
   );

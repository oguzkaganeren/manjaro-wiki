# Manjaro Wiki
## MediaWiki

MediaWiki is a free and open-source wiki software package written in PHP. It
serves as the platform for Wikipedia and the other Wikimedia projects, used
by hundreds of millions of people each month. MediaWiki is localised in over
350 languages and its reliability and robust feature set have earned it a large
and vibrant community of third-party users and developers.

MediaWiki is:

* feature-rich and extensible, both on-wiki and with hundreds of extensions;
* scalable and suitable for both small and large sites;
* simple to install, working on most hardware/software combinations; and
* available in your language.

MediaWiki is the result of global collaboration and cooperation. The CREDITS
file lists technical contributors to the project. The COPYING file explains
MediaWiki's copyright and license (GNU General Public License, version 2 or
later). Many thanks to the Wikimedia community for testing and suggestions.
## Running localy

```
pacman -S php mysql
```
> Select mariaDB


```
mysql_install_db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
```

```
systemctl enable mysqld
systemctl start mysqld
```

```
mysql_secure_installation
```
If you get `ERROR 1698 (28000): Access denied for user 'root'@'localhost'`, run with `sudo`.

**Enable iconv and mysql on php**
`nano /etc/php/php.ini`
remote `;` for `iconv`
```
extension=iconv
extension=pdo_mysql
extension=mysqli
```
Then to run PHP
```
php -S localhost:8000 -t public_html/
```
Change public_html with your project folder. Open `http://localhost:8000/index.php` on your browser.

When installing, you can get `ERROR 1698 (28000): Access denied for user 'root'@'localhost'`. You can solve that
! Don't do that at the server ;) It clears the mysql root password.
```
sudo mysql -u root -p
ALTER USER 'root'@'localhost' IDENTIFIED BY '';
```

### Clone Files
`git clone https://gitlab.manjaro.org/oguzkaganeren/manjaro-wiki`, then run `php -S localhost:8000 -t manjaro-wiki/`. Open `http://localhost:8000/` and start to installation according to your database and user information. When the installation is done, it will generate a `LocalSettings.php` file. Put it in `manjaro-wiki` folder. Stop your php server.

The `LocalSettings.php` file has default settings. You should change the some settings with the `ManjaroLocalSettings.php` (**stated** as `Paste below codes to your local settings`) **Don't paste all content of ManjaroLocalSettings, only paste you need(or below of stated)**. Then, Run `php manjaro-wiki/maintenance/update.php`. Re-start php server using `php -S localhost:8000 -t manjaro-wiki/`

### Import dump contents and images

```
php maintenance/importImages.php /Path/To/wiki_images/
php maintenance/importDump.php < /Path/To/All_Pages.xml
php maintenance/rebuildrecentchanges.php 
php maintenance/initSiteStats.php 

```


## Extensions and Configs

* (Categorytree)[https://www.mediawiki.org/wiki/Extension:CategoryTree#Usage]
* (CodeEditor)[https://www.mediawiki.org/wiki/Extension:CodeEditor]
* (MultimediaViewer)[https://www.mediawiki.org/wiki/Reading/Multimedia/Media_Viewer]
* (PageImages)[https://www.mediawiki.org/wiki/Extension:PageImages]
* (SyntaxHighlight)[https://www.mediawiki.org/wiki/Extension:SyntaxHighlight#Usage]
* (WikiEditor)[https://www.mediawiki.org/wiki/Extension:WikiEditor]
* (FontAwesome)[https://www.mediawiki.org/wiki/Extension:FontAwesome#Usage]
* (Babel)[https://www.mediawiki.org/wiki/Extension:Babel#Usage]
* (CLDR)[https://www.mediawiki.org/wiki/Extension:CLDR#Usage_and_parameters]
* (CleanChanges)[https://www.mediawiki.org/wiki/Extension:CleanChanges]
* (LocalisationUpdate)[https://www.mediawiki.org/wiki/Extension:LocalisationUpdate]
* (Translation)[https://www.mediawiki.org/wiki/Help:Extension:Translate#Further_reading_and_tutorials]
* (UniversalLanguageSelector)[https://www.mediawiki.org/wiki/Universal_Language_Selector]
* (TitleBlacklist)[https://www.mediawiki.org/wiki/Extension:TitleBlacklist#Usage]
* (SpamBlacklist)[https://www.mediawiki.org/wiki/Extension:SpamBlacklist]
* (ConfirmEdit)[https://www.mediawiki.org/wiki/Extension:ConfirmEdit]
* (CheckUser)[https://www.mediawiki.org/wiki/Extension:CheckUser]
* (ParserFunctions)[https://www.mediawiki.org/wiki/Help:Extension:ParserFunctions]
* (Interwiki)[https://www.mediawiki.org/wiki/Extension:Interwiki]
* (TitleKey)[https://www.mediawiki.org/wiki/Extension:TitleKey]
* (Lockdown)[https://www.mediawiki.org/wiki/Extension:Lockdown]
* (AbuseFilter)[https://www.mediawiki.org/wiki/Extension:AbuseFilter]
* (Nuke)[https://www.mediawiki.org/wiki/Extension:Nuke]
* (CodeMirror)[https://www.mediawiki.org/wiki/Extension:CodeMirror]
* (Thanks)[https://www.mediawiki.org/wiki/Extension:Thanks]
* (Notifications)[https://www.mediawiki.org/wiki/Notifications]
* (TranslationNotifications)[https://www.mediawiki.org/wiki/Extension:TranslationNotifications]
* (MobileFrontend)[https://www.mediawiki.org/wiki/Extension:MobileFrontend]
* (CookieWarning)[https://www.mediawiki.org/wiki/Extension:CookieWarning]
* (Preloader)[https://www.mediawiki.org/wiki/Extension:Preloader]
* (Moderation)[https://www.mediawiki.org/wiki/Extension:Moderation]





Use `<translate> </translate>` tags to want to translate a page. After saving the page, you can see mark the page for translation link on the page. 
(Detailed Information)[https://www.mediawiki.org/wiki/Help:Extension:Translate/Page_translation_example]

To show available language list: `<languages />`

We have speacial page(Special:Translate, Special:MessageGroupStats, Special:LanguageStats) thanks to the extension.

A user needs translator privilege to translate a page (`Special:UserRights`).


``` <syntaxhighlight lang="python" style="border:3px dashed blue">
def quick_sort(arr):
	less = []
	pivot_list = []
	more = []
	if len(arr) <= 1:
		return arr
	else:
		pass
</syntaxhighlight>
```
* (Preloader)[https://www.mediawiki.org/wiki/Extension:Preloader]

Using the above example configuration, create the wiki page **Template:Boilerplate** and use the standard include tags to indicate which parts are the template and which are the generic wiki text:
```
<includeonly>
__TOC__

== Overview ==

[[Category:MyDefaultCategory]]
</includeonly>
<noinclude>
This template is included by the Preloader extension on new article creation.
</noinclude>
```


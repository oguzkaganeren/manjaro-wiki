# How does it set up on the server?

Firstly, clone the repo on your server with `git clone https://gitlab.manjaro.org/oguzkaganeren/manjaro-wiki`

Open docker-compose.yml and change some settings which are labeled as change it

Then, open terminal inside of it and `docker-compose up -d`. It is installed 3 docker which are manjaro-wiki_mediawiki_1, manjaro-wiki_database_1, manjaro-wiki_smtp_1.(names can be different)

After installing docker, open the http://$(docker-machine ip)

Start the mediawiki installiation, 

Select database type MariaDB, MySQL, or compatible

Database host: manjaro-wiki_database_1(control the name with docker ps)

Database name: what you set inside of docker compose yml

Database username: what you set inside of docker compose yml

Database password: what you set inside of docker compose yml

Name of wiki: Manjaro

Should select I'm bored already, just install the wiki.

Download the localsettings.php and update it according to ManjaroLocalSettings.php(DO NOT FORGET CHANGE ReCaptchaNoCaptcha KEY and wgSMTP host informations) which is at the inside of repo. 

Important: only copy below codes of `Paste below codes to your local settings *******` comment to localsettings.php after the $wgUpgradeKey 

Then save your localsettings.php and copy to docker with `docker cp LocalSettings.php manjaro-wiki_mediawiki_1:/var/www/html`

And `docker cp wiki_images/ manjaro-wiki_mediawiki_1:/var/www/html/wiki_images/`

`docker cp images/ manjaro-wiki_mediawiki_1:/var/www/html/`

`docker cp extensions/ manjaro-wiki_mediawiki_1:/var/www/html/`

`docker cp skins/ manjaro-wiki_mediawiki_1:/var/www/html/`

`docker cp manjaro-dump.xml manjaro-wiki_mediawiki_1:/var/www/html/manjaro-dump.xml`

Open docker bash with `docker exec -it manjaro-wiki_mediawiki_1 bash`

Run `php maintenance/update.php`

Then Run `php maintenance/importImages.php wiki_images/`(it takes a time)

After finish it, run `php maintenance/importDump.php < manjaro-dump.xml`(it takes a time too)

Then run `php maintenance/rebuildrecentchanges.php` and `php maintenance/initSiteStats.php`

The installation Done. 

Login with your admin user, you should have moderation notification approve all.

Then, Go Special Pages and User rights and find the FuzzyBot(It does not appear before translating a page. So I will handle for you.) and give it bot permission.

Don't worry the main page is not changed after dump installation, you can see the recent changes of the page all content. It needs to revert the last change.

# Enable https
`docker exec -it manjaro-wiki_mediawiki_1 bash`
```
apt-get update
apt-get install certbot
apt-get install python-certbot-apache
certbot --apache -d wiki.manjaro.org -d www.wiki.manjaro.org
```



